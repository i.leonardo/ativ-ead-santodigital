import { Vue } from 'nuxt-property-decorator';

interface Indexable {
  [key: string]: any;
}

export const set = (property: string) => (
  store: Indexable, payload: string|number|boolean|object
) => {
  if (property.indexOf('.') > -1) {
    const [index, key] = property.split('.');
    Vue.set(store[index], key, payload);
  } else {
    Vue.set(store, property, payload);
  }
};

export const toggle = (property: string) => (store: Indexable) => {
  Vue.set(store, property, !store[property]);
};
