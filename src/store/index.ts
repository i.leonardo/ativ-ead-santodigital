import { set, toggle } from '@/utils/vuex';

export const state = () => ({
  dark: false,
  drawer: true,
  layout: false,
});

export const mutations = {
  toggleDark: toggle('dark'),
  setDrawer: set('drawer'),
  setLayout: set('layout'),
};
